import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'gmkdb',
  connector: 'mysql',
  url: '',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'root@1234*',
  database: 'gmkb_db'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class GmkdbDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'gmkdb';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.gmkdb', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
