import {DefaultCrudRepository} from '@loopback/repository';
import {OrderItem, OrderItemRelations} from '../models';
import {GmkdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OrderItemRepository extends DefaultCrudRepository<
  OrderItem,
  typeof OrderItem.prototype.id,
  OrderItemRelations
> {
  constructor(
    @inject('datasources.gmkdb') dataSource: GmkdbDataSource,
  ) {
    super(OrderItem, dataSource);
  }
}
