import {DefaultCrudRepository} from '@loopback/repository';
import {FoodItem, FoodItemRelations} from '../models';
import {GmkdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FoodItemRepository extends DefaultCrudRepository<
  FoodItem,
  typeof FoodItem.prototype.id,
  FoodItemRelations
> {
  constructor(
    @inject('datasources.gmkdb') dataSource: GmkdbDataSource,
  ) {
    super(FoodItem, dataSource);
  }
}
