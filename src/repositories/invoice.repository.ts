import {DefaultCrudRepository} from '@loopback/repository';
import {Invoice, InvoiceRelations} from '../models';
import {GmkdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class InvoiceRepository extends DefaultCrudRepository<
  Invoice,
  typeof Invoice.prototype.id,
  InvoiceRelations
> {
  constructor(
    @inject('datasources.gmkdb') dataSource: GmkdbDataSource,
  ) {
    super(Invoice, dataSource);
  }
}
