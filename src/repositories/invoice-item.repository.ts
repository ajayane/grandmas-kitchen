import {DefaultCrudRepository} from '@loopback/repository';
import {InvoiceItem, InvoiceItemRelations} from '../models';
import {GmkdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class InvoiceItemRepository extends DefaultCrudRepository<
  InvoiceItem,
  typeof InvoiceItem.prototype.id,
  InvoiceItemRelations
> {
  constructor(
    @inject('datasources.gmkdb') dataSource: GmkdbDataSource,
  ) {
    super(InvoiceItem, dataSource);
  }
}
