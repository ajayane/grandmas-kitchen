import {DefaultCrudRepository} from '@loopback/repository';
import {Offer, OfferRelations} from '../models';
import {GmkdbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OfferRepository extends DefaultCrudRepository<
  Offer,
  typeof Offer.prototype.id,
  OfferRelations
> {
  constructor(
    @inject('datasources.gmkdb') dataSource: GmkdbDataSource,
  ) {
    super(Offer, dataSource);
  }
}
