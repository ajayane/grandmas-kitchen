import {Entity, model, property} from '@loopback/repository';

@model()
export class ComboItem extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  combo_id: number;

  @property({
    type: 'number',
    required: true,
  })
  food_item_id: number;


  constructor(data?: Partial<ComboItem>) {
    super(data);
  }
}

export interface ComboItemRelations {
  // describe navigational properties here
}

export type ComboItemWithRelations = ComboItem & ComboItemRelations;
