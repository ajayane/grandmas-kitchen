import {Entity, model, property} from '@loopback/repository';

@model()
export class InvoiceItem extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  invoice_id: number;

  @property({
    type: 'number',
    required: true,
  })
  item_id: number;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'number',
    required: true,
  })
  discount: number;


  constructor(data?: Partial<InvoiceItem>) {
    super(data);
  }
}

export interface InvoiceItemRelations {
  // describe navigational properties here
}

export type InvoiceItemWithRelations = InvoiceItem & InvoiceItemRelations;
