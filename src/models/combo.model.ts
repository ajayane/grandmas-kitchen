import {Entity, model, property} from '@loopback/repository';

@model()
export class Combo extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  status: string;


  constructor(data?: Partial<Combo>) {
    super(data);
  }
}

export interface ComboRelations {
  // describe navigational properties here
}

export type ComboWithRelations = Combo & ComboRelations;
