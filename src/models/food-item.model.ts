import {Entity, model, property} from '@loopback/repository';

@model()
export class FoodItem extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'string',
    required: true,
  })
  ingredients: string;


  constructor(data?: Partial<FoodItem>) {
    super(data);
  }
}

export interface FoodItemRelations {
  // describe navigational properties here
}

export type FoodItemWithRelations = FoodItem & FoodItemRelations;
