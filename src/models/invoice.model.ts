import {Entity, model, property} from '@loopback/repository';

@model()
export class Invoice extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  total_amount: number;

  @property({
    type: 'string',
    required: true,
  })
  currency: string;

  @property({
    type: 'number',
    required: true,
  })
  discount: number;
  @property({
    type: 'number',
    required: true,
  })
  delivery_charge: number;

  @property({
    type: 'number',
    required: true,
  })
  tax: number;

  @property({
    type: 'string',
    required: true,
  })
  mode_of_payment: string;

  @property({
    type: 'string',
    required: true,
  })
  status: string;

  @property({
    type: 'string',
    required: true,
  })
  payment_status: string;


  constructor(data?: Partial<Invoice>) {
    super(data);
  }
}

export interface InvoiceRelations {
  // describe navigational properties here
}

export type InvoiceWithRelations = Invoice & InvoiceRelations;
