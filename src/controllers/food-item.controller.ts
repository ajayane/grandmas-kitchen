import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {FoodItem} from '../models';
import {FoodItemRepository} from '../repositories';

export class FoodItemController {
  constructor(
    @repository(FoodItemRepository)
    public foodItemRepository : FoodItemRepository,
  ) {}

  @post('/food-items', {
    responses: {
      '200': {
        description: 'FoodItem model instance',
        content: {'application/json': {schema: getModelSchemaRef(FoodItem)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodItem, {
            title: 'NewFoodItem',
            exclude: ['id'],
          }),
        },
      },
    })
    foodItem: Omit<FoodItem, 'id'>,
  ): Promise<FoodItem> {
    return this.foodItemRepository.create(foodItem);
  }

  @get('/food-items/count', {
    responses: {
      '200': {
        description: 'FoodItem model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(FoodItem) where?: Where<FoodItem>,
  ): Promise<Count> {
    return this.foodItemRepository.count(where);
  }

  @get('/food-items', {
    responses: {
      '200': {
        description: 'Array of FoodItem model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(FoodItem, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(FoodItem) filter?: Filter<FoodItem>,
  ): Promise<FoodItem[]> {
    return this.foodItemRepository.find(filter);
  }

  @patch('/food-items', {
    responses: {
      '200': {
        description: 'FoodItem PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodItem, {partial: true}),
        },
      },
    })
    foodItem: FoodItem,
    @param.where(FoodItem) where?: Where<FoodItem>,
  ): Promise<Count> {
    return this.foodItemRepository.updateAll(foodItem, where);
  }

  @get('/food-items/{id}', {
    responses: {
      '200': {
        description: 'FoodItem model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(FoodItem, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(FoodItem, {exclude: 'where'}) filter?: FilterExcludingWhere<FoodItem>
  ): Promise<FoodItem> {
    return this.foodItemRepository.findById(id, filter);
  }

  @patch('/food-items/{id}', {
    responses: {
      '204': {
        description: 'FoodItem PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FoodItem, {partial: true}),
        },
      },
    })
    foodItem: FoodItem,
  ): Promise<void> {
    await this.foodItemRepository.updateById(id, foodItem);
  }

  @put('/food-items/{id}', {
    responses: {
      '204': {
        description: 'FoodItem PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() foodItem: FoodItem,
  ): Promise<void> {
    await this.foodItemRepository.replaceById(id, foodItem);
  }

  @del('/food-items/{id}', {
    responses: {
      '204': {
        description: 'FoodItem DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.foodItemRepository.deleteById(id);
  }
}
