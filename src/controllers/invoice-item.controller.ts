import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {InvoiceItem} from '../models';
import {InvoiceItemRepository} from '../repositories';

export class InvoiceItemController {
  constructor(
    @repository(InvoiceItemRepository)
    public invoiceItemRepository : InvoiceItemRepository,
  ) {}

  @post('/invoice-items', {
    responses: {
      '200': {
        description: 'InvoiceItem model instance',
        content: {'application/json': {schema: getModelSchemaRef(InvoiceItem)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(InvoiceItem, {
            title: 'NewInvoiceItem',
            
          }),
        },
      },
    })
    invoiceItem: InvoiceItem,
  ): Promise<InvoiceItem> {
    return this.invoiceItemRepository.create(invoiceItem);
  }

  @get('/invoice-items/count', {
    responses: {
      '200': {
        description: 'InvoiceItem model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(InvoiceItem) where?: Where<InvoiceItem>,
  ): Promise<Count> {
    return this.invoiceItemRepository.count(where);
  }

  @get('/invoice-items', {
    responses: {
      '200': {
        description: 'Array of InvoiceItem model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(InvoiceItem, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(InvoiceItem) filter?: Filter<InvoiceItem>,
  ): Promise<InvoiceItem[]> {
    return this.invoiceItemRepository.find(filter);
  }

  @patch('/invoice-items', {
    responses: {
      '200': {
        description: 'InvoiceItem PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(InvoiceItem, {partial: true}),
        },
      },
    })
    invoiceItem: InvoiceItem,
    @param.where(InvoiceItem) where?: Where<InvoiceItem>,
  ): Promise<Count> {
    return this.invoiceItemRepository.updateAll(invoiceItem, where);
  }

  @get('/invoice-items/{id}', {
    responses: {
      '200': {
        description: 'InvoiceItem model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(InvoiceItem, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(InvoiceItem, {exclude: 'where'}) filter?: FilterExcludingWhere<InvoiceItem>
  ): Promise<InvoiceItem> {
    return this.invoiceItemRepository.findById(id, filter);
  }

  @patch('/invoice-items/{id}', {
    responses: {
      '204': {
        description: 'InvoiceItem PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(InvoiceItem, {partial: true}),
        },
      },
    })
    invoiceItem: InvoiceItem,
  ): Promise<void> {
    await this.invoiceItemRepository.updateById(id, invoiceItem);
  }

  @put('/invoice-items/{id}', {
    responses: {
      '204': {
        description: 'InvoiceItem PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() invoiceItem: InvoiceItem,
  ): Promise<void> {
    await this.invoiceItemRepository.replaceById(id, invoiceItem);
  }

  @del('/invoice-items/{id}', {
    responses: {
      '204': {
        description: 'InvoiceItem DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.invoiceItemRepository.deleteById(id);
  }
}
