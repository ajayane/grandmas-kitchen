# grandmas-kitchen

**setting up project**
prerequisite: install node js
clone the project
go to project root folder
open a command prompt
run: `npm install`
once completed
run: `npm i -g @loopback/cli`

install mysql:
create a database with name: gmkb_db
update the username and password in the src\datasources\gmkdb.datasource.ts file

now run: `npm start`
the DB tables will be auto created.
the server will be started at [localhost:3000](localhost:3000)

open browser and hit: [localhost:3000/explore](localhost:3000/explore)
see the api list and request fonrmats to use it.
